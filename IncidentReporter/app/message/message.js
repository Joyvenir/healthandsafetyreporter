﻿(function() {
    'use strict';
    var controllerId = 'message';
    angular.module('app').controller(controllerId, ['common','$http','$scope', message]);

    function message(common,$http,$scope) {
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);

        var vm = this;
        vm.title = 'Message';
        vm.location = "";
        vm.description = "";
        vm.includeScience = false;
        vm.sendToMe = false;
        vm.reset = function () {
            $scope.$broadcast('show-errors-reset');
            vm.title = '';
            vm.location = '';
            vm.description = '';
            vm.sendToMe = false;
            vm.includeScience = false;
        };
        vm.sendEmail = function () {
            $scope.$broadcast('show-errors-check-validity');
            if ($scope.messageForm.$invalid) { return; }
            var emailMessage = {
                includeScience: vm.includeScience,
                sendToMe: vm.sendToMe,
                location: vm.location,
                description: vm.description
            }
            $http.post('api/report', emailMessage);
            vm.reset();
        }
        activate();
        function activate() {
            common.activateController([], controllerId)
                .then(function() { return true; });
        }
    }
})()