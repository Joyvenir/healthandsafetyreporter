﻿using System.Collections.Generic;
using System.Net.Mail;
using System.Runtime.Remoting.Messaging;
using System.Web;
using System.Web.Http;
using System.Web.Services.Description;

namespace IncidentReporter.Controllers
{
    public class ReportController : ApiController
    {
        // GET api/<controller>
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        public void Post(MailInfo messageValues)
        {
            //post an email to me
            var reportedBy = HttpContext.Current.User.Identity.Name.Split('\\')[1];
            var message = new MailMessage
            {
                From = new MailAddress(reportedBy + @"@clsb.org.uk"),
                Subject = "Health and Safety Notice: " + messageValues.Location,
                Body = "<p>Location: " + messageValues.Location +"</p>" + "<p>Reported By: "+ reportedBy+"</p>" +"<p>Description: " + messageValues.Description+"</p>",
                IsBodyHtml = true
            };
            if (messageValues.SendToMe)
            {
                message.To.Add(new MailAddress(reportedBy+"@clsb.org.uk"));
            }
            if (messageValues.IncludeScience)
            {
                message.To.Add(new MailAddress("hrsj@clsb.org.uk"));
            }
            message.To.Add(new MailAddress("gsg@clsb.org.uk"));
            message.To.Add(new MailAddress("jfh@clsb.org.uk"));
            //var server = new SmtpClient()
            //{
            //    PickupDirectoryLocation = @"C:\temp\emails\healthandsafety",
            //    DeliveryMethod = SmtpDeliveryMethod.SpecifiedPickupDirectory
            //};
            var emailer = new SmtpClient()
            {
                Host = "clsb-ex2010-01",
                Port = 25,
                DeliveryMethod = SmtpDeliveryMethod.Network
            };
            emailer.Send(message);
        }

    }

    public class MailInfo
    {
        public bool SendToMe { get; set; }
        public bool IncludeScience { get; set; }
        public string Description { get; set; }
        public string Location { get; set; }
    }
}